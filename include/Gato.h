#ifndef GATO_H
#define GATO_H

#include <Mascota.h>


class Gato : public Mascota
{
    public:
        Gato();
        Gato(std::string _nombre, int _anios): Mascota(_nombre, _anios) {};
        int queEdadTenes (); // redefincion del metodo heredado
        virtual ~Gato();

    protected:

    private:
};

#endif // GATO_H
