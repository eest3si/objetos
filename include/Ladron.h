#ifndef LADRON_H
#define LADRON_H
#include "CajaFuerte.h"

class Ladron
{
    public:
        Ladron();
        virtual ~Ladron();
        void robar (CajaFuerte*);
        float getRobado (); // getter de robado

    protected:

    private:
        CajaFuerte* objetivo;
        float robado;
};

#endif // LADRON_H
