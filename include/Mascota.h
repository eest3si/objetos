#ifndef MASCOTA_H
#define MASCOTA_H
#include <string>

class Mascota
{
    public:
        Mascota();
        Mascota (std::string, int); // nombre y edad iniciales
        virtual ~Mascota();
        std::string getNombre () {return nombre;} // getter
        int queEdadTenes ();

    protected:
        std::string nombre;
        int anios;

    private:
};

#endif // MASCOTA_H
