#ifndef PERRO_H
#define PERRO_H

#include <Mascota.h>

class Perro : public Mascota  // Perro hereda de Mascota
{
    public:
        Perro();
        Perro(std::string _nombre, int _anios): Mascota(_nombre, _anios) {};
        int queEdadTenes (); // redefincion del metodo heredado
        virtual ~Perro();

    protected:

    private:
};

#endif // PERRO_H
