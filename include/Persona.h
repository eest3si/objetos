#ifndef PERSONA_H
#define PERSONA_H
#include <string>
#include "CajaFuerte.h"
#include "Mascota.h"

class Persona
{
    public:
        Persona(); // Constructor por defecto
        Persona (std::string, std::string, float, CajaFuerte*); // constructor parametrizado
        virtual ~Persona();
        std::string dondeVivis();
        std::string comoTeLlamas();
        void mudarse (std::string);
        float dineroTotal (); // devuelve la cantidad total de dinero de la persona
        void cobrar (float); // ahorra el 40% de lo cobrado
        bool puedeComprarAlgoQueCuesta (float); // determina si puede comprar algo
        void reasignarCajaFuerte (CajaFuerte*);
        void adoptar (Mascota*);
        std::string comoSeLlamaTuMascota ();
        int queEdadTieneTuMascota ();

    protected:

    private:
        std::string ciudad;
        std::string nombre;
        CajaFuerte* miCajaFuerte; // relacion de asociacion
        Mascota* miMascota;      // relacion de asociacion
        float efectivo;
};

#endif // PERSONA_H
