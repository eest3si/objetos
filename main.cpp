#include <iostream>
using namespace std;
#include "Persona.h"
#include "Perro.h"

int main()
{
    Persona unaPersona;  // instancio una persona "por defecto"
    Perro unPerro ("Ayudante de Santa", 4);

    unaPersona.adoptar (&unPerro);
    cout << unaPersona.comoSeLlamaTuMascota () << endl;
    cout << unaPersona.queEdadTieneTuMascota () << endl;

    return 0;
}
