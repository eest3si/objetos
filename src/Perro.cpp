#include "Perro.h"

Perro::Perro()
{
    //ctor
}

Perro::~Perro()
{
    //dtor
}

int Perro::queEdadTenes ()
// El primer a�o de vida del perro equivale a 15 a�os de edad en humanos
// mientras que el segundo a�o solo equivale a 9 a�os humanos.
// Luego, cada a�o canino equivale a 4 a�os humanos.
{
    int edad = 0;

    if (anios == 1)
        edad = 15;
    else
        if (anios == 2)
            edad = 24;
        else
            edad = 24 + (anios - 2) * 4;

    return edad;
}
