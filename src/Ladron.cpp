#include "Ladron.h"

Ladron::Ladron()
{
    robado = 0;
}

Ladron::~Ladron()
{
    //dtor
}

float Ladron::getRobado ()
{
    return robado;
}

void Ladron::robar (CajaFuerte* unaCajaFuerte)
{
    objetivo = unaCajaFuerte; // conozco la caja fuerte
    robado += objetivo -> cuantoHay(); // tomo lo que tiene
    objetivo -> vaciar (); // la vacio
}
