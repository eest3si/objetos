#include "Gato.h"

Gato::Gato()
{
    //ctor
}

Gato::~Gato()
{
    //dtor
}

int Gato::queEdadTenes ()
// El primer a�o de vida del gato equivale a 10 a�os de edad en humanos
// mientras que el segundo a�o solo equivale a 9 a�os humanos.
// Luego, cada a�o canino equivale a 4 a�os humanos.
{
    int edad = 0;

    if (anios == 1)
        edad = 10;
    else
        if (anios == 2)
            edad = 19;
        else
            edad = 19 + (anios - 2) * 4;

    return edad;
}
