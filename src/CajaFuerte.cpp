#include "CajaFuerte.h"

CajaFuerte::CajaFuerte()
{
    importe = 0; // caja inicialmente vacia
    cantVeces = 0; // nueva (sin uso)
}

CajaFuerte::~CajaFuerte()
{
    //dtor
}

void CajaFuerte::guardar (float unValor)
{
    importe += unValor; // guardo reservas
    cantVeces ++; // llevo la cuenta de las operaciones realizadas
}

void CajaFuerte::vaciar ()
{
    importe = 0;  // vacio la caja
}

float CajaFuerte::cuantoHay ()
{
    return importe;
}
